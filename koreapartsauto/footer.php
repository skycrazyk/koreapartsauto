    <footer class="footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8">
            <div class="d-flex mb-3">
              <i class="fas fa-map-marked-alt footer__icon"></i>
              <div>г. Оренбург, ул.Юных Ленинцев,<br>22/1, ТК Просторный, 2 этаж.</div>
            </div>
            <div class="d-flex mb-3">
              <i class="fas fa-mobile-alt footer__icon"></i>
              <div class="row">
                <div class="col-sm-6">
                  <div>8 (3532) 69-30-80</div>
                  <div>8 (932) 536-30-80</div>
                </div>
                <div class="col-sm-6">
                  <div>8 (3532) 69-50-30</div>
                  <div>8 (932) 536-50-30</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 mb-2">
            <div class="mb-2">
              <i class="fab fa-vk footer__icon"></i><a href="https://vk.com/koreapartsauto">Мы Вконтакте</a>
            </div>
            <div>
              <i class="far fa-envelope footer__icon"></i><a href="mailto:koreapartsauto@yandex.ru">koreapartsauto@yandex.ru</a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="d-flex mb-3">
              <i class="far fa-file-alt footer__icon"></i>
              <div>
                ОГРНИП 313565815000100 <br>
                ИНН 565001793346 <br>
                ИП Прохоров Андрей Юрьевич
              </div>
            </div>
          </div>
        </div>
        <div class="footer__kia"></div>
      </div>
    </footer>
    
    <a  
      class="btn btn-success fixed-bottom d-sm-none btn-block callback-btn btn-lg" 
      href="tel:+79325363080"
      onclick="ym(25649192, 'reachGoal', 'callToKoreaPartsAuto');"
    >
      <i class="fas fa-phone-volume"></i>
      Позвонить в KoreaPartsAuto
    </a>

    <!-- Yandex.Metrika counter --> 
    <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(25649192, "init", { id:25649192, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/25649192" style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
    <!-- /Yandex.Metrika counter -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    
    <?php wp_footer(); ?>
  </body>
</html>
