<?php
add_theme_support( 'title-tag' );

add_action( 'after_setup_theme', 'theme_register_nav_menu' );
function theme_register_nav_menu() {
	register_nav_menu( 'MainMenu', 'MainMenu' );
}

// Изменяет основные параметры меню
add_filter( 'wp_nav_menu_args', 'filter_wp_menu_args' );
function filter_wp_menu_args( $args ) {
	if ( $args['theme_location'] === 'MainMenu' ) {
		$args['container']  = false;
		$args['menu_class'] = 'navbar-nav ml-auto';
		$args['echo'] = 'true';
	}
	return $args;
}

// Изменяем атрибут class у тега li
add_filter( 'nav_menu_css_class', 'filter_nav_menu_css_classes', 10, 4 );
function filter_nav_menu_css_classes( $classes, $item, $args, $depth ) {
	if ( $args->theme_location === 'MainMenu' ) {
		$classes = [
			'nav-item',
		];
		if ( $item->current ) {
			$classes[] = 'active';
		}
	}
	return $classes;
}

// Добавляем классы ссылкам
add_filter( 'nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4 );
function filter_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
	if ( $args->theme_location === 'MainMenu' ) {
		$atts['class'] = 'nav-link';
	}
	return $atts;
}

// woocommerce
function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );