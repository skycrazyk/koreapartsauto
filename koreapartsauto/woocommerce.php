<?php
/* Template Name: woocommerce */
get_header(); ?>

  <main class="pt-4 pb-4 container-fluid">
    <?php woocommerce_content(); ?>
	</main>

<?php get_footer(); ?>
