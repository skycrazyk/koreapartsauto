<!doctype html>
<html lang="ru">
	<head>
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  	<meta name="format-detection" content="telephone=no">
  	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

  	<!-- Bootstrap CSS -->
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    
    <!-- Theme CSS -->
  	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
		
		<?php wp_head(); ?>
	</head>
	<body>
  	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    	<div class="container-fluid">
      	<a class="navbar-brand" href="/">KoreaPartsAuto</a>
			
      	<button 
      	  class="navbar-toggler" 
      	  type="button" 
      	  data-toggle="collapse" 
      	  data-target="#navbarSupportedContent" 
      	  aria-controls="navbarSupportedContent" 
      	  aria-expanded="false" 
      	  aria-label="Toggle navigation"
      	>
      	  <span class="navbar-toggler-icon"></span>
        </button>

  			<div class="collapse navbar-collapse" id="navbarSupportedContent">
  				<?php	wp_nav_menu(['theme_location'  => 'MainMenu']);	?>
  			</div>
      </div>
  	</nav>
		
		<div class="py-2 contacts">
		  <div class="container-fluid contacts__container">
  			<div class="row">
  			  <div class="col-sm-8 mb-2 mb-sm-0">
    				<div class="d-flex">
    				  <i class="fas fa-mobile-alt mr-2 contacts__icon"></i>
    				  <div class="row">
      					<div class="col-sm-6">
      					  <div>8 (3532) 69-30-80</div>
      					  <div>8 (932) 536-30-80</div>
      					</div>
      					<div class="col-sm-6">
      					  <div>8 (3532) 69-50-30</div>
      					  <div>8 (932) 536-50-30</div>
      					</div>
    				  </div>
    				</div>
  			  </div>
  			  <div class="col-sm-4">
    				<div class="d-flex">
    				  <i class="far fa-clock mr-2 contacts__icon"></i>
    				  <div>
    					<div>10.00-18.00</div>
    					<div>Выходные: ВС</div>
    				  </div>
    				</div>
  			  </div>
  			</div>
		  </div>
		</div>
		